module.exports = {
  apps: [{
    "name":"Azamorn.ovh",
    "script": "./src/main.ts",
    "interpreter": "ts-node",
    "watch": 'src/'
  }]
};
