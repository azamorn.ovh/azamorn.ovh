import express = require('express');
import cors = require('cors');
import path = require('path');
import fs = require('fs');
import dotenv = require('dotenv');
import NodeCache = require('node-cache');
import Joi = require('@hapi/joi');

const config_schema = Joi.object({
    APP_PORT: Joi.number().required(),
    API_PORT: Joi.number().required(),
    APP_HOST: Joi.string().uri().required(),
    API_HOST: Joi.string().uri().required()
});

export class AzamornOVH {
    db: any;
    cache = new NodeCache();
    config: any;
    web: express.Application;
    api: express.Application;
    modules: Object;
    startup_time: number; 
    constructor(){  
        this.startup_time = Date.now();  
        this.db = require('monk')('localhost/azamornovh');
        this.cache = new NodeCache()
        config_schema.validateAsync(dotenv.config().parsed).then((data) => {
            this.config = dotenv.config().parsed;
            this.web = express(); //Handles all html, css, js serving
            this.web.use('/resources', express.static('resources'));
            this.api = express(); //Handles all api routes
            this.api.set('trust proxy', 1); //Important for proxying behind Nginx
            this.api.use(cors()); //Important since the api lives on a different subdomain, and could potentially be used by other sites.
            //Modules can each add routes to the main site.
            this.loadModules().then(() => {
                Promise.all([
                    new Promise((listening) => {
                        this.web.listen(this.config.APP_PORT, () => {
                            console.log(`Application is running on port ${this.config.APP_PORT}`);
                            listening();
                        });
                    }),
                    new Promise((listening) => {
                        this.api.listen(this.config.API_PORT, () => {
                            console.log(`API is running on port ${this.config.API_PORT}`);
                            listening();
                        });
                    })
                ]).then(() => {
                    console.log(`Azamorn.ovh is live\r\nStartup Time:${Date.now() - this.startup_time}ms`)
                })
            })
        }).catch((err) => {
            throw new Error(err);
        });
    }

    loadModules(){
        this.modules = {};
        return new Promise(async (modules_loaded) => {
            let imported = 0, total = 0, files = fs.readdirSync('./modules',{ withFileTypes: true }),
            module_loaded = () => {
                imported++;
                if(imported == total){
                    modules_loaded();
                }
            };
            for(let i = 0, n = files.length; i < n; i++){
                if(files[i].isDirectory()){
                    total++;
                }
            }
            for(let i = 0, n = files.length; i < n; i++){
                if(files[i].isDirectory()){
                    let moduleName = files[i].name;
                    import(`../modules/${moduleName}/index.ts`).then((module : any) => {
                        this.modules[moduleName] = new module[Object.keys(module)[0]](this);
                        try{
                            this.modules[moduleName].init().then(module_loaded);
                        }
                        catch(err){
                            console.error(`Initializing ${moduleName} failed.`, err);
                        }
                    }).catch((err) => {
                        console.error(err);
                    })
                }
            }
        });
    }
}
exports.default = new AzamornOVH();